#!/usr/bin/python
# A Fancy script to calculate the booths algorithm product on two signed binary numbers
# Shoutout to SimpleFlips

def TWO(a):
    # Function to take the two's complement of a number.
    # Makes sure it's a decimal and then ANDs it with all 1s,
    # (this is a handy way to take the two's complement)
    # then returns the last 8 bits.
    # Can find the two's complement of numbers up to 16 bits
    # but only returns  8 bits.
    a = int(a,2)
    b = format( -a & 0b1111111111111111,'08b' )
    return b

def operationCheck(a, b):
    # Simple function to take the LSB of the value
    # and the booth bit and use it to find which operation
    # to execute on this number, this is checked in the main
    # code.
    if a == "0":
        #0X
        if b == "0":
            #00
            return("00 - NONE")
        else:
            #01
            return("01 - ADD")
    else:
        #1X
        if b == "0":
            #10
            return("10 - SUB")
        else:
            #11
            return("11 - NONE")

def ADD(a, b):
    # Simple function to add two numbers and return the 
    # binary value of it, removing overflow.
    c = format(int(a,2) + int(b,2),'08b')
    return c[-8:]

def NONE():
    # An operation to do nothing.
    # Literally the only reason it is here is for 
    # easy understanding.
    return True

def SHIFT(a, b, c):
    # Function to shift binary values one bit to the right.
    d = str(a) + str(b) + str(c)
    if d[:1] == "0":
        # Normal shift for positive signed numbers.
        d = format(int(d,2) >> 1,'017b')
    else:
        # Special shift for negative signed numbers.
        # Basically a normal shift but with a 1 instead
        # of a 0.
        d = str(1)+format(int(d,2) >> 1,'016b')
    return(d[0:8],d[8:16],d[16:17])

def binaryCheck(a):
    # Overly complicated function to convert binary
    # inputs to decimal values and decimal inputs into
    # binary values.
    # Honestly I'm pretty sure it's not needed,
    # but I'm afraid to get rid of it now.
    if len(a) >= 8:
        if a[:1] == "1":
            a = TWO(a)
            a = -int(a,2)
        else:
            a = int(a,2)

    a = int(a)
    if a < 0:
        a = TWO(format(abs(a),'08b'))
    else:
        a = format(a,'08b')
    
    return a


# Taking inputs
decimalFactor = input('Enter the first number: ')
decimalMultiplier = input('Enter the second number: ')
# Converting the inputs to signed binary values
binaryFactor = binaryCheck(decimalFactor)
binaryMultiplier = binaryCheck(decimalMultiplier)
# Set up the equation
print(binaryFactor,"*",binaryMultiplier,"\n")


# First binary block
binaryA = format(0,'08b')
# Second binary block
binaryB = binaryMultiplier
# Booth but
# Don't remember what it's actually called, but I like booth bit
boothBit = "0"
# For loop to iterate 8 times for the binary inputs
for x in range(1,9):
    # Prints the step so I don't get lost
    print("Step ", x)
    # Takes the last bit of the second block
    LSB = (binaryB[7:8])
    # Checks the booth bit and the LSB to determine the operation
    operation = operationCheck(LSB,boothBit)
    print(binaryA,binaryB,boothBit,"\t",operation)
    # This loop looks at the operation code and either adds, subs, 
    # or does nothing.
    if operation == "00 - NONE":
        NONE()
        print(binaryA,binaryB,boothBit)
    elif operation == "01 - ADD":
        # Adds the factor to the number.
        binarySum = ADD(binaryA, binaryFactor)
        print(binaryA+"+"+binaryFactor+"="+binarySum)
        binaryA = binarySum
    elif operation == "10 - SUB":
        # Adds the two's complement to the number, 
        # effectively subtracting but with adders.
        binaryDif = ADD(binaryA, TWO(binaryFactor))
        print(binaryA+"+"+TWO(binaryFactor)+"="+binaryDif)
        binaryA = binaryDif
    elif operation == "11 - NONE":
        NONE()
        print(binaryA,binaryB,boothBit)
    else:
        # This happens if the operation isn't one of the four.
        # This should never happen.
        print("oh no") 
    # Print out the shift line
    print(binaryA,binaryB,boothBit,"\t Shift")
    # This one takes all three numbers, shifts them, then sends
    # the shifted value back into the variables. 
    binaryA, binaryB, boothBit = SHIFT(binaryA,binaryB,boothBit)
    print(binaryA,binaryB,boothBit)

# Creates the answer in binary.
binaryAnswer = binaryA + binaryB
# Prints the exact answer in binary.
print("Answer: " + binaryAnswer)
# This converts the the binary to decimal,
# accounting for two's complement numbers.
if binaryAnswer[:1] == "0":
    print(">>      " + str(int(binaryAnswer,2)))
else:
    print(">>     -" + str(int(TWO(binaryAnswer),2)))
